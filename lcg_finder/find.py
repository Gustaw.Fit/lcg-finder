import os
import urllib

import gitlab
import progressbar

import configuration as config


def main(gl):

    print("Starting to connect to Gitlab SAAS")
    gl.auth()
    # get all users - require all users to skip user-private repositories
    print("Getting all repos")
    group = gl.groups.get(config.gitlab["zoopla_group_id"])
    all_gitlab_users = group.members_all.list(all=True)
    all_gitlab_projects = group.projects.list(simple=True, all=True, archived=False, include_subgroups=True)

    all_gitlab_non_personal_projects = filter_all_personal_projects(all_gitlab_projects, all_gitlab_users)

    print("Finding all LCGs in Gitlab")
    find_and_print_lcg(gl, all_gitlab_non_personal_projects)


def filter_all_personal_projects(all_gitlab_projects, all_gitlab_users):

    end_result = []
    for project in all_gitlab_projects:
        if is_project_personal(all_gitlab_users, project):
            continue

        end_result.append(project)

    return end_result


def is_project_personal(all_gitlab_users, gitlab_project):
    for user in all_gitlab_users:
        target_url = gitlab_project.web_url

        # Checking if project is personal or not
        project_group = urllib.parse.urlparse(target_url).path.split("/")[1]
        if user.username == project_group:
            return True

    return False


def get_main_branch(project) -> str | None:
    ref_branch = None
    try:
        project.branches.get(config.gitlab["ref_branch"])
        ref_branch = config.gitlab["ref_branch"]
    except gitlab.exceptions.GitlabGetError:
        pass

    try:
        project.branches.get(config.gitlab["ref_branch_2"])
        ref_branch = config.gitlab["ref_branch_2"]
    except gitlab.exceptions.GitlabGetError:
        pass

    return ref_branch


def find_and_print_lcg(gl, all_gitlab_projects):
    widgets = [
        " [",
        progressbar.Timer(format="elapsed time: %(elapsed)s"),
        "] ",
        progressbar.Bar("*"),
        " (",
        progressbar.ETA(),
        ") ",
    ]

    bar = progressbar.ProgressBar(max_value=len(all_gitlab_projects), widgets=widgets).start()

    i = 0
    projects = "\n"

    for project in all_gitlab_projects:
        manageable_project = gl.projects.get(project.id, lazy=True)
        ref_branch = get_main_branch(manageable_project)
        if ref_branch is not None:
            try:
                manageable_project.files.get(file_path=config.gitlab["lcg_config_file_name"], ref=ref_branch)
                projects += project.web_url + "\n"
            except gitlab.exceptions.GitlabGetError:
                pass

        bar.update(i)
        i += 1

    print("\n All LCG list:")
    print(projects)


def find():
    main(gitlab.Gitlab(config.gitlab["url"], oauth_token=os.environ["PYTHON_GITLAB_SAAS_TOKEN"]))
