# lcg-finder

## Description
This repository helps to get LCG list (https://zpgltd.atlassian.net/wiki/spaces/ZA/pages/23823188113/Legacy+Codebase+Gateway+-+LCG+101).

It is searching for 'lcg-configuration.openapi.yml' in the root repo of each Gitlab project.

#### Tokens
You will need to first create a gitlab personal access token to run this locally. Follow: https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html.

Once you have you personal tokens, add it them your .bash_profile file in /home/[username], example below:

```
[...]
export PYTHON_GITLAB_SAAS_TOKEN="yourpersonaltoken"
[...]
```

#### To run

```
pip install poetry
poetry install
make run
```

#### Example output
It takes about 14mins to run.

```
Starting to connect to Gitlab SAAS
Getting all repos
Finding all LCGs in Gitlab
 [elapsed time: 0:14:13] |************************************************************************************************** | (ETA:   0:00:00)
 All LCG list:


https://gitlab.com/zoopla/consumer/api-geo-location
https://gitlab.com/zoopla/consumer/api-saved-properties
https://gitlab.com/zoopla/consumer/api-agent
https://gitlab.com/zoopla/consumer/api-rental-yield
https://gitlab.com/zoopla/consumer/api-property-search
https://gitlab.com/zoopla/consumer/api-listing-premium-data
https://gitlab.com/zoopla/consumer/api-listing-extension
https://gitlab.com/zoopla/consumer/api-area-sold-prices
https://gitlab.com/zoopla/consumer/api-listing-alerts
https://gitlab.com/zoopla/consumer/api-discover-cms
https://gitlab.com/zoopla/consumer/templates/api-legacy-codebase-template
https://gitlab.com/zoopla/consumer/api-zpro-info
https://gitlab.com/zoopla/consumer/api-market-stats
https://gitlab.com/zoopla/consumer/api-apple-authentication
https://gitlab.com/zoopla/zoopla-software/pa/permissions-service
https://gitlab.com/zoopla/consumer/api-consumer

 [elapsed time: 0:14:13] |***************************************************************************************************| (Time:  0:14:13)
```
