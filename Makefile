lint:
	poetry run flake8

black:
	poetry run black -v .

mypy:
	poetry run mypy .

run:
	cd lcg_finder && poetry run find

test: black mypy lint
